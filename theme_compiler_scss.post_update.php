<?php

/**
 * @file
 * Use this file to define post-update hooks as needed.
 *
 * This method for writing updates should only be used if the limitations on
 * hook_update_N() cause an undue burden.
 *
 * Sequential identifiers should NOT be used for hook_post_update_NAME()
 * implementations unless ordering between multiple implementations is required.
 *
 * @see hook_post_update_NAME()
 *   For the best practices surrounding the usage of post update hooks.
 */

/**
 * Install the new SCSS compiler module.
 */
function theme_compiler_scss_post_update_0001_install_compiler_scss() {
  \Drupal::service('module_installer')->install([
    'compiler_scss',
  ]);
}

/**
 * Recompile assets provided by the Theme Compiler module.
 */
function theme_compiler_scss_post_update_0002_recompile_assets() {
  \Drupal::service('theme_compiler.compiler')->compileAssets();
}

/**
 * Uninstall the deprecated SCSS theme compiler module.
 */
function theme_compiler_scss_post_update_0003_uninstall_theme_compiler_scss() {
  \Drupal::service('module_installer')->uninstall([
    'theme_compiler_scss',
  ]);
}
